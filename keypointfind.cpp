/*
Keypoint-based object detection
Created by Maxim Mikhaylov, 2018
*/

#include <stdio.h>
#include <iostream>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/nonfree/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "opencv2/flann/flann.hpp"

using namespace std;
using namespace cv;

int main()
{
	VideoCapture cap(0);
	Mat img, imGray;
	Mat reference_image = imread("sample.jpg", IMREAD_GRAYSCALE);
	
	if(!reference_image.data)
	{
		std::cout << "couldn't read the reference image" << std::endl;
		return -1; 
	}

	int minHessian = 1500; // Sensitivity of detector
	SURF detector(minHessian);
	SurfDescriptorExtractor extractor;

	// Compute reference stuff
	vector<KeyPoint> reference_keypoints;
	Mat reference_descriptors;
	detector.detect(reference_image, reference_keypoints);
	extractor.compute(reference_image, reference_keypoints, reference_descriptors);

	while(1)
	{
		cap >> img;

		cvtColor(img, imGray, COLOR_BGR2GRAY);
		vector<KeyPoint> keypoints;
		Mat descriptors;
		detector.detect(imGray, keypoints);
		extractor.compute(imGray, keypoints, descriptors);

		//Match using FLANN matcher
		FlannBasedMatcher matcher;
		std::vector<DMatch> matches;
		matcher.match(reference_descriptors, descriptors, matches);
		double max_dist = 0; double min_dist = 100; double avg_dist = 0;
		
		// Calculate max and min distances between keypoints
		for(int i = 0; i < reference_descriptors.rows; i++)
		{ 
			double dist = matches[i].distance;
		    if(dist < min_dist) min_dist = dist;
		    if(dist > max_dist) max_dist = dist;
		}
		printf("Max dist : %f \n", max_dist);
		printf("Min dist : %f \n", min_dist);

		// Find good matches
		std::vector<DMatch> good_matches;
		
		for(int i = 0; i < reference_descriptors.rows; i++)
		{ 
			if(matches[i].distance <= max(2*min_dist, 0.02))
			{ 
				good_matches.push_back(matches[i]);
				avg_dist += matches[i].distance;
			}
		}
		avg_dist /= matches.size();

		Point2f avg_point = Point2f();
		for(int i = 0; i < good_matches.size(); i++)
		{
			avg_point += keypoints[matches[i].trainIdx].pt * (matches[i].distance / avg_dist / matches.size());
		}

		// Draw only good matches
		Mat img_matches;
		drawMatches(reference_image, reference_keypoints, img, keypoints,
		            good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
		            vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);
		
		//draw a circle at the center of the
		circle(img, avg_point, 20, Scalar(0, 0, 255));

		// Show good matches
		imshow("Good Matches", img_matches);
		imshow("Object position", img);
		waitKey(1);
	}
	return 0;
}