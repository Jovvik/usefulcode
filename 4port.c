/*
Two-sided 4port NXT communication
Created by Maxim Mikhaylov, 2018
*/

ubyte input[4] = {0,0,0,0};
char output[4] = {250,0,0,0};

task read()
{
	int num = 0;
	nxtEnableHSPort();
	nxtHS_Mode = hsRawMode;
	nxtSetHSBaudRate(9600);
	wait1Msec(1000);
	while(1)
	{
		num = nxtGetAvailHSBytes();
		if (num == 4)
		{
			nxtReadRawHS(input, num);
		}
	}
}

task send()
{
	nxtEnableHSPort();
	nxtHS_Mode = hsRawMode;
	nxtSetHSBaudRate(9600);
	wait1Msec(1000);
	while(1)
	{
		nxtWriteRawHS(output, 4, 0);
		wait1Msec(1000);
	}
}

task main()
{
	bool flag = false;
	startTask(send);
	startTask(read);
	clearTimer(timer1);
	clearTimer(timer2);
	while(1)
	{
		output[1] = time100(timer1) % 255;
		output[2] = random(254);
		if (input[3] > 0 && !flag)
		{
			nxtDisplayTextLine(4, "error");
			flag = true;
			playSound(soundException);
		}
		nxtDisplayTextLine(1, "i %i %i %i %i", input[0], input[1], input[2], input[3]);
		nxtDisplayTextline(2, "o %i %i %i %i", output[0], output[1], output[2], output[3]);
		nxtDisplayTextLine(3, "%i", time100(timer1) / 10);
	}
}