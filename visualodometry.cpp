/*
Keypoint-based visual odometry with homography
Created by Maxim Mikhaylov, 2018
*/

// Not finished yet, Женя is still writing this

#include <iostream>
#include "opencv2/opencv.hpp"
#include "opencv2/core/core.hpp"
#include "opencv2/features2d/features2d.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "opencv2/calib3d/calib3d.hpp"
#include "opencv2/nonfree/nonfree.hpp"
#include "chrono"

using namespace cv;

int main()
{
    typedef std::chrono::high_resolution_clock Time;

    Mat img, imgray, imgprev;
    VideoCapture cap(0);

    if(!cap.isOpened())
    { 
        std::cout<< " Error reading images " << std::endl; 
        return -1; 
    }

    // feature detector
    int minHessian = 500; //sensitivity of detector
    SurfFeatureDetector detector(minHessian);
    std::vector<KeyPoint> keypoints, keypointsprev;
    
    // descriptor extractor
    SurfDescriptorExtractor extractor;
    Mat descriptors, descriptorsprev;

    // descriptor matcher
    FlannBasedMatcher matcher;
    std::vector<DMatch> matches; 
    
    auto start_time = Time::now();
    int frameCount = 0;

    while(1)
    { 
        cap >> img;
        cvtColor(img, imggray, COLOR_BGR2GRAY); // save the bgr version in img to show it

        detector.detect(img_scene, keypoints);
        extractor.compute(img_scene, keypoints, descriptors);

        //  Skip the matching for the first frame since we don't have previous keypoints
        if (frameCount==0)
        {
            keypoints.swap(keypointsprev);
            descriptorsprev = descriptors.clone();
            continue;
        }

        matcher.match(descriptors, descriptorsprev, matches);

        double max_dist = 0; double min_dist = 100;

        // Calculation of max and min distances between keypoints
        for(int i = 0; i < descriptors.rows; i++)
        { 
            double dist = matches[i].distance;
            if(dist < min_dist) min_dist = dist;
            if(dist > max_dist) max_dist = dist;
        }

        // Draw only "good" matches (i.e. whose distance is less than 3*min_dist)
        std::vector<DMatch> good_matches;

        for(int i = 0; i < descriptors.rows; i++)
        { 
            if(matches[i].distance < 3*min_dist)
            { 
                good_matches.push_back(matches[i]); 
            }
        }

        Mat img_matches;
        drawMatches(imggray, keypoints, imgprev, keypoints,
                    good_matches, img_matches, Scalar::all(-1), Scalar::all(-1),
                    vector<char>(), DrawMatchesFlags::NOT_DRAW_SINGLE_POINTS);

        // Localize the object
        std::vector<Point2f> obj;
        std::vector<Point2f> scene;

        for(int i = 0; i < good_matches.size(); i++)
        {
            // Get the keypoints from the good matches
            obj.push_back(keypoints_object[ good_matches[i].queryIdx ].pt);
            scene.push_back(keypoints_scene[ good_matches[i].trainIdx ].pt);
        }

        Mat H = findHomography(obj, scene, CV_RANSAC);

        // Get the corners from the image_1 (the object to be "detected")
        std::vector<Point2f> obj_corners(4);
        obj_corners[0] = cvPoint(0,0); obj_corners[1] = cvPoint(img_object.cols, 0);
        obj_corners[2] = cvPoint(img_object.cols, img_object.rows); obj_corners[3] = cvPoint(0, img_object.rows);
        std::vector<Point2f> scene_corners(4);

        perspectiveTransform(obj_corners, scene_corners, H);

        // Draw lines between the corners (the mapped object in the scene - image_2)
        line(img_matches, scene_corners[0] + Point2f(img_object.cols, 0), scene_corners[1] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[1] + Point2f(img_object.cols, 0), scene_corners[2] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[2] + Point2f(img_object.cols, 0), scene_corners[3] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);
        line(img_matches, scene_corners[3] + Point2f(img_object.cols, 0), scene_corners[0] + Point2f(img_object.cols, 0), Scalar(0, 255, 0), 4);

        // Show detected matches
        imshow("Good Matches & Object detection", img_matches);

        frameCount++;
        std::cout << "avg fps: " << (float)frameCount / ((Time::now() - start_time).count() / 10e8) << std::endl;

        keypoints.swap(keypointsprev);
        descriptorsprev = descriptors.clone();

        waitKey(1);
    }
    return 0;
}