// Code for sending data from linux to arduino via COM port
// Note that symbol '\n' is number 10, therefore you can't send it
#include "iostream"
#include <unistd.h>

int main()
{
    FILE *file;
    file = fopen("/dev/ttyUSB0", "w");
    int output = 0;
    while(1)
    {
        std::cin >> output;
        fprintf(file, "%c\n", output, output);
    }
    fclose(file);
}