/*
Color-based object detection in c++
For better explanation check colorfind.py, this is its' conversion to c++
TODO: test this
Created by Maxim Mikhaylov, 2018
*/

Point2f colorDetection(Mat img, Scalar lowerBound, Scalar upperBound, Scalar lowerBound2 = Scalar(-1, -1, -1), Scalar upperBound2 = Scalar(-1, -1, -1), int threshold = 1))
{
	//create all mats
	Mat imgHSV, imgGray;
	Mat1b mask1, mask2, mask;

	//convert to HSV and grayscale
	cvtColor(img, imgHSV, COLOR_BGR2HSV);
	cvtColor(img, imgGray, COLOR_BGR2GRAY);

	//filter HSV image by color
	inRange(imgHSV, lowerBound, upperBound, mask1);

    //if two pairs of bounds are used
    if (lowerBound2.val[0] != -1)
    {
        inRange(imgHSV, lowerBound2, upperBound2, mask2);
	    //add the mats together
	    mask = mask1 | mask2;
    }
	else
	{
		mask = mask1;
	}

	//delete small dots in background
	erode(mask, mask, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
	dilate(mask, mask, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

	//fill small holes in foreground
	dilate(mask, mask, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));
	erode(mask, mask, getStructuringElement(MORPH_ELLIPSE, Size(5, 5)));

	//create a moments structure
	Moments oMoments = moments(mask);

	//for cleaner acess
	double dM01 = oMoments.m01;
	double dM10 = oMoments.m10;
	double dArea = oMoments.m00;

	//if there are a lot of red pixels the object is in the image
	if (dArea > threshold)
	{
		//calculate coordintes of the object
		int posX = dM10 / dArea;
		int posY = dM01 / dArea;
        return Point2f(posx, posY);
	}
    return Point2f(-1, -1)
}