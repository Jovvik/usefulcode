// Code for receiving data from COM port on Arduino

#define Y_LPWM 6
#define Y_RPWM 7

void setup()
{
    Serial.begin(9600);
    pinMode(Y_LPWM, OUTPUT);
    pinMode(Y_RPWM, OUTPUT);
}
 
int input = 0, motorVal = 0;
 
void loop()
{
    if(Serial.available()>0)
    {
        input = Serial.read();
        if (input != 10) // '\n' symbol exeption
          motorVal = input;
    }

    analogWrite(Y_LPWM, motorVal);
    analogWrite(Y_RPWM, LOW);
    
    delay(1000);
}
