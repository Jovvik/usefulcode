/*
Bluetooth NXT<-PC communication
Created by Maxim Mikhaylov, 2018
*/

ubyte data[10];

task btcomp()
{
	while(1)
	{
		while (cCmdMessageGetSize(mailbox1) < 1)
		{
			nxtDisplayTextLine(3, "size %i", 7);
		}
		nxtDisplayTextLine(3, "size %i", (mailbox1));
		cCmdMessageRead(data, 10, mailbox1);
		playSound(soundBeepBeep);

		wait10Msec(2);
		nxtDisplayTextLine(6, "%i %i %i %i %i %i %i %i %i", data[0], data[1], data[2], data[3], data[4], data[5], data[6], data[7], data[8]);
	}
}

task main()
{
	startTask(btcomp);
	wait1Msec(1000000000000000000);
}