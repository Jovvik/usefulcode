#!bin/bash

echo 'This is a machine learning library installer script'
echo 'Coded by Maxim Mikhaylov'

mkdir $HOME/libs
mkdir $HOME/libs/tmp

LAMBDA_REPO=$HOME/libs/tmp
wget -O${LAMBDA_REPO} https://lambdalabs.com/static/misc/lambda-stack-repo.deb
sudo dpkg -i ${LAMBDA_REPO}
sudo apt-get update 
sudo apt-get install -y lambda-stack-cuda