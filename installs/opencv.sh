#! /bin/bash

echo 'This is an opencv installer script'
echo 'Coded by Maxim Mikhaylov'
echo ''

download_opencv() {
    echo 'Downloading opencv, version ' $OPENCV_VERSION
    curl -o $HOME/libs/tmp/opencv-$OPENCV_VERSION.zip https://codeload.github.com/opencv/opencv/zip/$OPENCV_VERSION
}

download_opencv_contrib() {
    echo 'Downloading opencv-contrib version ' $OPENCV_VERSION
    curl -o $HOME/libs/tmp/opencv_contrib-$OPENCV_VERSION.zip https://codeload.github.com/opencv/opencv_contrib/zip/$OPENCV_VERSION
}

echo 'Choose opencv version, default is 3.4.3'
echo 'No clue why, but 4.0.0 doesnt work'
read OPENCV_VERSION
if [[ ! -n $OPENCV_VERSION ]]; then
    echo 'Falling back to default opencv version - 3.4.3'
    OPENCV_VERSION='3.4.3'
fi

echo 'Install opencv-contrib? y - yes, h - help, anything else - no'
read REPLY
if [[ $REPLY =~ ^[Yy]$ ]]; then
    echo 'Will install contrib'
    INSTALL_CONTRIB=true
fi
if [[ $REPLY =~ ^[Nn]$ ]]; then
    echo 'Will not install contrib'
    INSTALL_CONTRIB=false
fi
if [[ $REPLY =~ ^[Hh]$ ]]; then
    echo 'Opencv-contrib is a set of additional packages for opencv.'
    echo 'Installing it will take some time and space on your hard drive,'
    echo 'but it will enhance abilities of opencv, enabling many functions.'
    echo 'Now you gotta go from the start cuz im a lazy cunt'
    exit 0
fi

echo 'Installing dependecies with apt-get'
sudo apt-get -qq install -y build-essential g++
sudo apt-get -qq install -y cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
sudo apt-get -qq install -y python-dev python-numpy libtbb2 libtbb-dev libjpeg-dev libpng-dev libtiff-dev libjasper-dev libdc1394-22-dev
sudo apt-get -qq install -y libv4l-dev libqt4-dev libgtk-3-dev

mkdir "$HOME/libs"
mkdir "$HOME/libs/tmp"

# just in case
rm "$HOME/libs/opencv-$OPENCV_VERSION/CMakeLists.txt"

# uncomment if something really doesn't wanna work
# rm -rf $HOME/libs/opencv-$OPENCV_VERSION
# rm -rf $HOME/libs/opencv_contrib-$OPENCV_VERSION

if [[ -f $HOME/libs/tmp/opencv-$OPENCV_VERSION.zip ]]; then
    echo 'Opencv archive was found. Should it be downloaded anyway?'
    read REPLY
    [[ ! $REPLY =~ ^[Yy]$ ]] || download_opencv
else
    download_opencv
fi

if [[ "$INSTALL_CONTRIB" = true ]]; then
    if [[ -f $HOME/libs/tmp/opencv_contrib-$OPENCV_VERSION.zip ]]; then
        echo 'Opencv-contrib archive was found. Should it be downloaded anyway?'
        read REPLY
        [[ ! $REPLY =~ ^[Yy]$ ]] || download_opencv_contrib
    else
        download_opencv_contrib
    fi
fi

echo 'Unzipping the archive(s)'
unzip -qq -o "$HOME/libs/tmp/opencv-$OPENCV_VERSION.zip" -d $HOME/libs
if [[ "$INSTALL_CONTRIB" = true ]]; then
    unzip -qq -o "$HOME/libs/tmp/opencv_contrib-$OPENCV_VERSION.zip" -d $HOME/libs
fi

OPENCV_PATH=$HOME/libs/opencv-$OPENCV_VERSION
OPENCV_CONTRIB_PATH=$HOME/libs/opencv_contrib-$OPENCV_VERSION
mkdir "$OPENCV_PATH/build"

cd "$OPENCV_PATH/build" || return

if [[ "$INSTALL_CONTRIB" = true ]]; then
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_CUDA=ON -D WITH_CUBLAS=ON -D WITH_TBB=ON -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF -D BUILD_EXAMPLES=OFF -D OPENCV_EXTRA_MODULES_PATH="$OPENCV_CONTRIB_PATH/modules" ..
else
    cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local -D WITH_CUDA=ON -D WITH_CUBLAS=ON -D WITH_TBB=ON -D WITH_V4L=ON -D WITH_QT=ON -D WITH_OPENGL=ON -D BUILD_PERF_TESTS=OFF -D BUILD_TESTS=OFF -D BUILD_EXAMPLES=OFF ..
fi

make -j6

sudo make install